import React from 'react';
import PropTypes from 'prop-types';

import Portal from 'portal';
import {UiClose} from 'ui';

import Loader from 'loader';
import {getModifiers} from 'libs/component';

// https://stackoverflow.com/questions/36985738/how-to-unmount-unrender-or-remove-a-component-from-itself-in-a-react-redux-typ

import './Alert.scss';

import Icon from 'icon';
import IconError from 'icon/error.svg';
import IconWarning from 'icon/warning.svg';
import IconInfo from 'icon/info.svg';
import IconSuccess from 'icon/success.svg';

const ICONS = {
	error: IconError,
	warning: IconWarning,
	info: IconInfo,
	success: IconSuccess
};

/**
 * Alert
 * @description [Description]
 * @example
  <div id="Alert"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Alert, {
        title : 'Example Alert'
    }), document.getElementById("Alert"));
  </script>
 */
class Alert extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'alert';

		this.state = {
			timer: props.timer,
			isActive: props.isOpen
		};
	}

	onClose = ev => {
		this.clear();

		this.setState(
			{
				isActive: false
			},
			() => {
				const {onDismiss} = this.props;

				if (onDismiss) {
					onDismiss();
				}
			}
		);
	};

	clear() {
		if (this.tick) {
			clearTimeout(this.tick);
			this.tick = null;
		}
	}

	refresh() {
		const {isActive, timer} = this.state;

		this.clear();

		if (isActive) {
			if (timer) {
				this.tick = setTimeout(() => {
					this.tick = null;
					this.onClose();
				}, timer);
			}
		}
	}

	componentDidMount() {
		this.refresh();
	}

	renderAlert() {
		const {isInline, isDismissable, children, type, isMini} = this.props;

		const isLoader = type === 'loader';

		const canDismiss = isDismissable && !isLoader;

		const className = getModifiers(this.baseClass, [
			type,
			isInline ? 'inline' : 'fixed',
			canDismiss ? 'dismissable' : null
		]);

		const icon = ICONS[type] || null;
		const hasIcon = !isMini || isLoader || (!isMini && !isLoader && icon);

		return (
			<div className={className} ref={component => (this.component = component)}>
				<div className={`${this.baseClass}__container`}>
					<div className={`${this.baseClass}__body`}>
						{canDismiss && (
							<div className={`${this.baseClass}__close`}>
								<UiClose className={'close'} onClick={this.onClose} />
							</div>
						)}

						{hasIcon && (
							<div className={`${this.baseClass}__icon`}>
								{isLoader ? <Loader theme="light" /> : <Icon icon={icon} />}
							</div>
						)}

						<div className={`${this.baseClass}__main`}>{children}</div>
					</div>
				</div>
			</div>
		);
	}

	render() {
		const {isInline} = this.props;
		const {isActive} = this.state;

		if (!isActive) {
			return null;
		}

		if (isInline) {
			return this.renderAlert();
		}

		return <Portal>{this.renderAlert()}</Portal>;
	}
}

Alert.defaultProps = {
	timer: null,
	type: 'default',
	isOpen: true,
	isMini: false,
	isDismissable: false,
	isInline: true,
	children: null,
	onDismiss: null
};

Alert.propTypes = {
	timer: PropTypes.number,
	type: PropTypes.string,
	isMini: PropTypes.bool,
	isOpen: PropTypes.bool,
	isDismissable: PropTypes.bool,
	isInline: PropTypes.bool,
	onDismiss: PropTypes.func,
	children: PropTypes.oneOfType([PropTypes.array, PropTypes.string, PropTypes.object])
};

export default Alert;
